package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{

    List<FridgeItem> ItemsInFridge= new ArrayList<>();
    @Override
    public int nItemsInFridge() {
    
        return ItemsInFridge.size();
    }

    @Override
    public int totalSize() {
        return 21;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (ItemsInFridge.size() <= 20) {
            ItemsInFridge.add(item);
            return true;
        }
        return false;
        }

    @Override
    public void takeOut(FridgeItem item) {
        if (ItemsInFridge.contains(item)) {
            ItemsInFridge.remove(item);
        }
        else {
            throw new NoSuchElementException();
        }
        
    }

    @Override
    public void emptyFridge() {
        ItemsInFridge.clear();  
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredFood = new ArrayList<>();
        for(FridgeItem item: ItemsInFridge){
            if (item.hasExpired() == true){
                expiredFood.add(item);
            }
            
        }
        for (FridgeItem item: expiredFood){
            ItemsInFridge.remove(item);
        }
        return expiredFood;
    }
    
}
